# ShareLaTeX Helm Chart

This is a Helm chart for deploying ShareLaTeX to a Kubernetes cluster.

## Prerequisites

Before deploying the ShareLaTeX Helm chart, you will need to have the following installed on your machine:

- [Helm](https://helm.sh/)
- [Kubectl](https://kubernetes.io/docs/tasks/tools/)

You will also need to have a Kubernetes cluster set up, with a working `kubeconfig` file.

## Deployment

To deploy the ShareLaTeX Helm chart, follow these steps:

1. Clone this repository:

    ```
    git clone https://github.com/your/repo.git
    ```

2. Navigate to the `chart` directory:

    ```
    cd chart
    ```

3. Edit the `values.yaml` file to configure the deployment. See the comments in the file for details on the available options.

4. Install the chart using Helm:

    ```
    helm install sharelatex . --namespace my-namespace
    ```

   Replace `my-namespace` with the name of the namespace you want to deploy to.

5. Wait for the deployment to complete. You can check the status of the deployment using the `kubectl get pods` command.

6. Access the ShareLaTeX instance by opening a web browser and navigating to `http://<ip-address>:<port>`. Replace `<ip-address>` and `<port>` with the IP address and port of the Kubernetes service.

## GitLab CI

This repository includes a GitLab CI file (`./.gitlab-ci.yml`) that can be used to automatically test the ShareLaTeX Helm chart. The CI file uses a Kind cluster to spin up a Kubernetes cluster for testing.

To use the GitLab CI file, follow these steps:

1. Make sure that your GitLab runner is set up to use Docker as the executor.

2. Add the following variables to your GitLab project:

   - `DOCKER_HOST`: Set to `tcp://docker:2375/`
   - `DOCKER_DRIVER`: Set to `overlay2`
   - `KUBECONFIG`: Set to `/root/kubeconfig.yaml`
   - `APP_NAMESPACE`: Set to the name of the Kubernetes namespace you want to deploy to.

3. Commit and push your changes to the repository.

4. The GitLab CI pipeline will automatically trigger and run the tests. You can view the results in the GitLab UI.
